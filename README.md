# raspi-k8s-nodesetup
Shell script for setting up a Raspberry PI Kubernetes node featuring:

* Packages upgraded - We developers build bugs in our software to keep ourselfs occupied
* Hostnames configured - Needs to be configured by you!
* Locales configured - The idiot that invented a different metric system should be... well you know what I mean
* Timezone configured - Time flies when you're having fun
* Swap disabled - Due to https://github.com/kubernetes/kubeadm/issues/610
* Docker installed - Sensible choice
* Kubernetes installed - kubeadm, kubectl, kubeblablablabla...

# Prerequisites
A Raspberry Pi flashed with Raspbian (this stuff is tested against Stretch Lite). Also it might be wise to place an empty file named `ssh` in `/boot` in order to gain SSH access to your Pi for headless setup (remember default user `pi`, password `raspberry`).

# How to Run
Login on the PI you wish to be the master node as user `pi` and execute the following steps.

## Step 1. Configure nodesetup
```bash
sudo curl -o /etc/nodesetup.conf https://bitbucket.org/sequlo/raspi-k8s-nodesetup/raw/HEAD/example.nodesetup.conf
sudo vi /etc/nodesetup.conf
``` 

## Step 2. Run nodesetup
```bash
sudo curl -o /usr/local/sbin/nodesetup https://bitbucket.org/sequlo/raspi-k8s-nodesetup/raw/HEAD/nodesetup
sudo nodesetup
```
